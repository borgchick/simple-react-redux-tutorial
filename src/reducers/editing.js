const editingReducer = (state = [], action) => {
  switch (action.type) {
    case 'EDITING':
      return {
          is_editing: true
      }
    case 'NOT_EDITING':
      return {
          is_editing: false
      }
    default:
      return state
  }
};

export default editingReducer;
