import { combineReducers } from 'redux';
import editingReducer from './editing.js';

export default combineReducers({
  editingReducer
})
