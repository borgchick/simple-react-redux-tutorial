export const editing = () => ({
  type: 'EDITING',
  editing: true
});

export const not_editing = () => ({
  type: 'NOT_EDITING',
  editing: false
})
