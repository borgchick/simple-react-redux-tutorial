import { connect } from 'react-redux'
import { editing, not_editing } from '../../actions'
import EditingState from '../presentation/EditState'

const mapStateToProps = state => ({
  is_editing: state.editingReducer.is_editing
});

const mapDispatchToProps = dispatch => ({
  set_editing: () => dispatch(editing()),
  set_not_editing: () => dispatch(not_editing())
})

export default connect(mapStateToProps, mapDispatchToProps)(EditingState)
