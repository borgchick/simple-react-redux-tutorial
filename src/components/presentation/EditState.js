import React from 'react';
const EditingState = ({is_editing, set_editing, set_not_editing }) => (
  <div>
    Editing state is: {is_editing ? "Editing" : "Not Editing"}<br/>
    <button onClick={()=>set_editing()}>Edit</button>
    <button onClick={()=>set_not_editing()}>Not Editing</button>
  </div>
)
export default EditingState;
