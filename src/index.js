import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers'
import TestApp from './TestApp'

const store = createStore(rootReducer)

render(
  <Provider store={store}>
    <TestApp />
  </Provider>,
  document.getElementById('root')
)
